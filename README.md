# Usage

Provides routes by default:  
GET `/` *test status*  
GET `/uploads/*` *get a converted file, example `/uploads/pdf_pptx_Standarts.pdf`*  
WS `:4001/` *listen for a file here*  

__default port is__ `:4000`  
__websocket port is__ `:4001`

Stores uploaded and converted files in `/public/uploads`

Logs at output.log 

## Settings

by default port `:4001` may not be open. Needs to open it first!
2) change port in `/server/index.js`

## Setup

1) `npm i`
2) Download and install libreoffice dependency https://www.libreoffice.org/download/download/

### Usage
By default there is always 2 incoming messages before file:
1) First message, which saying server file name contain mask `file:`, after colon here goes filename, i.e `file:somefilename.pptx`;
2) Second message with mask `size:` tell server incoming file size in bytes *(im not sure)*, `size:24534567`;
3) Next messages will contain file chunks in Buffer type *(default binary nodejs type)* , by default server isn't handle it in any way, just wait until `end;` flag
4) Mask `end;` notes about ending a file transition and next try to convert the file;

### File TTL option
1. Added option to set file lifetime
2. Lifetime set in classic crontab option
3. Each file has its own task which starts countdown after file has been upload
4. Once task fired, it will log info and self destroy

You can manage presentation files lifetime through `config.presentation.fileTTL` option.
At this moment different envs contain different life spans for their file, so be sure 
that your dev is set to something like `dev`, `prod`, `local` and another envs due to `env-config`


#### Example of job definition:
 Cron syntax is very simple. So few more info you could find here
 https://opensource.com/article/17/11/how-use-cron-linux
 
 And use this if you wont mind about details
 https://crontab-generator.org/

### Start to develop:

* `npm start` check is service running: http://localhost:4000/, you must see the test page
- if you don't want to use Vault for config.json generation you can generate it by youself from env-config repo
- Go to env-config repo
- Generate config.json and fill the secret gaps with your own credentials
- Put the config.json file into the config/ folder inside the _presentation_ folder
- `npm start`

### Libre Office Options:

__--minimized__    keep startup bitmap minimized.  
__--invisible__    no startup screen, no default document and no UI.  
__--norestore__    suppress restart/restore after fatal errors.  
__--quickstart__   starts the quickstart service  
__--nologo__       don't show startup screen.  
__--nolockcheck__  don't check for remote instances using the installation  
__--nodefault__    don't start with an empty document  
__--headless__     like invisible but no userinteraction at all.  
__--help/-h/-?__  show this message and exit.  
__--version__      display the version information.  
__--writer__       create new text document.  
__--calc__         create new spreadsheet document.  
__--draw__         create new drawing.  
__--impress__      create new presentation.  
__--base__         create new database.  
__--math__         create new formula.  
__--global__       create new global document.  
__--web__          create new HTML document.  
__-o__             open documents regardless whether they are templates or not.  
__-n__             always open documents as new files (use as template).  
