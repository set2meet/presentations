const express = require('express');
const bodyParser = require('body-parser');
const { resolve } = require('path');
const log = require('../src/logger');

const https = require('https');

const app = express();

const isProduction = process.env.NODE_ENV === 'production';

function runHttpsServer(httpsPort, httpsOptions) {
  app.use(bodyParser.json());

  /**
   * Only for checking is service available at all
   */
  app.get('/', function (req, res, next) {

    if(!isProduction){
      log.info('HTTP: Some requests here!');
      res.sendFile(resolve('./public', 'index.html'));
      //res.status(200).send('ALL RIGHT HERE!');
    } else {
      res.status(200).send();
    }

  });

  /**
   * Using for return converted file
   */
  app.get('/download/:filename', function (req, res, next) {
    log.info(`Attempt to receive file! - ${resolve('../presentations/downloads/' + req.params.filename)}`);
    res.sendFile(resolve('../presentations/downloads/' + req.params.filename), function (err) {
      if (err) {
        log.error(`Can't find a file ${decodeURIComponent(req.path)}`);
        res.status(404).send('No presentation found!');
        next(err);
      }
    });
  });

  https.createServer(httpsOptions, app).listen(httpsPort, () => {
    log.info('HTTPS: Start listen server on ' + httpsPort);
  });
}

module.exports = runHttpsServer;
