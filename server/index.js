const runHttpServer = require('./http_server');
const runWSocketServer = require('./ws_server');
const init = require('../src/init');
const readConfig = require("env-config").readConfig;

async function startService() {
    const config = await readConfig();
    const {presentation, httpsOptions} = config;
    const {httpsPort, wsPort, downloadPath, uploadPath, fileTTL} = presentation;

    init(downloadPath, uploadPath);
    runHttpServer(httpsPort, httpsOptions);
    runWSocketServer(wsPort, uploadPath, downloadPath, httpsOptions, fileTTL);
}

startService();
