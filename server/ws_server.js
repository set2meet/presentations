const mime = require('mime-types');
const log = require('../src/logger');
const fs = require('fs');
const https = require('https');
const WebSocketServerIO = require('socket.io');
const {resolve, extname} = require('path');
const presentationToPDF = require('../src/converter');
const cron = require('node-cron');
const isExtensionValid = (ext, allowedTypes, allowedExtensions) => {
  if (!allowedTypes.includes(mime.lookup(ext))) {
    return allowedExtensions.includes(ext);
  }
  return true;
};

const isUploadValid = (file, allowedTypes) => {
  return allowedTypes.includes(mime.lookup(file));
};

const deleteFile = path => {
  fs.unlink(path, (err) => {
    if (err) throw err;
  })
};

const time_remaining = (date_provided) => new Date(date_provided) - new Date();

function runWSocketServer(wsPort, uploadPath, downloadPath, httpsOptions, fileTTL) {
  const httpsServer = https.createServer(httpsOptions).listen(wsPort, () => {
    log.info('WS: Start listening on  ' + wsPort);
  });

  const io = WebSocketServerIO(httpsServer);

  let file, presentationFilePath, stream, tempFile;
  let uploadsDir = resolve('./', downloadPath);

  const fileTasksMap = {};

  function scheduleDeleteFile(file){
    log.info(`Schedule deleting for ${file}`);

    return cron.schedule(fileTTL, () => {
      const fpath = resolve('./', downloadPath, file);

      fs.stat(fpath, function (err, stat) {
        if (!err) {
          try {
            deleteFile(fpath);
            const task = fileTasksMap[file];
            if(task){
              task.destroy();
              log.info(`Task has been completed for file ${file}!`)
            }

          } catch (e) {
            log.error(e.toString());
            const task = fileTasksMap[file];
            if(task){
              task.destroy();
              log.info(`Task has been removed for file ${file} as file didnt exists...`)
            }
          } finally {
            delete fileTasksMap[file];
          }
        } else {
          log.error(err.toString());
          log.info('File didnt exists... removing task');
          const task = fileTasksMap[file];
          if(task){
            task.destroy();
            log.info(`Task has been removed for file ${file} as file didnt exists...`)
            delete fileTasksMap[file];
          }
        }
      });
    }, {});
  }

  io.on('connection', (socket) => {
    console.log('connection established')

    socket.on('filename', function (f) { // f vor file *Anonymous.jpg*
      file = f;
      presentationFilePath = resolve('./', uploadPath, file);
      log.info(`WS: Server preparing to receive file ${file}, saving as ${presentationFilePath}`);

      stream = fs.createWriteStream(presentationFilePath);
      stream.on('open', () => {
        socket.emit('ready');
      });

      stream.on('error', () => {
        socket.emit('stream_error', 'end;');
        socket.disconnect();
      })
    });

    socket.on('filesize', function (fsize) {
      log.info(`WS: ${file} is ${fsize} Kbytes long!`);
    });

    socket.on('filechunks', (chunk) => {
      if (chunk) {
        log.info(`WS: Server getting chunks... Please, wait`);
        stream.write(chunk)
      } else {
        stream.end();
      }
    });

    socket.on('error', (e) => {
      log.info('WS: Some error here!', e.toString());
    });

    socket.on('end', () => {
      const ext = extname(presentationFilePath); // get extension
      stream.on('finish', () => {
        log.info(`WS: Server stopped receiving file ${file}`);
        fileTasksMap[file] = scheduleDeleteFile(file);

        if (ext !== '.pdf') {
          try {
            log.info(`WS: Trying convert file to PDF...`);
            let threshold = 10;
            const reminder = setInterval(() => {
              log.info(`File still converting, wait please... ${threshold} seconds`);
              threshold += 10;
            }, 10000);
            presentationToPDF(presentationFilePath, downloadPath)
              .then(
                () => {
                  log.info(`WS: File converted and ready to download!`);
                  log.info(`presentationToPDF() convert proccess ends in ~${threshold} seconds`);
                  socket.emit('done', 'end;');
                  socket.disconnect();
                  clearInterval(reminder)
                }
              ).catch((e) => {
              log.error(`presentationToPDF(): Error while conversing file... ${e}`);

              clearInterval(reminder);
              log.info('Socket closed! ' + e)
            })
          } catch (e) {
            log.error(`presentationToPDF(): Error while trying convert file... ${e}`);
            socket.emit('convert_error', 'end;');
            socket.disconnect()
          }
        } else {
          log.info(`WS: Wow! It's already PDF, so we just move file!`);
          fs.rename(presentationFilePath, resolve('./', downloadPath, file), () => {
            socket.emit('done', 'end;');
            socket.disconnect()
          })
        }

      });
    });
  });
}

module.exports = runWSocketServer;
