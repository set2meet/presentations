const log = require('./logger');
const { existsSync, mkdirSync } = require('fs');
const { resolve } = require('path');

/**
 * Here we resolve or create folders/files, combine variables, etc
 */

const init = (downloadPath, uploadPath) => {
  const DOWNLOAD_DIR = resolve('./', downloadPath);
  const UPLOAD_DIR = resolve('./', uploadPath);

  if (!existsSync(DOWNLOAD_DIR)){
    log.info(`FS: Folder for downloads been created: ${DOWNLOAD_DIR}`);
    mkdirSync(DOWNLOAD_DIR);
  }

  if (!existsSync(UPLOAD_DIR)){
    log.info(`FS: Folder for uploads been created: ${UPLOAD_DIR}`);
    mkdirSync(UPLOAD_DIR);
  }
};

module.exports = init;
