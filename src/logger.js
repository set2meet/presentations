const logger = require('log4js');

logger.configure({
  appenders: {
    logs: { type: 'file', filename: './output.log' },
    console: { type: 'console', layout: { type: "coloured" } }
  },
  categories: {
    default: { appenders: ['console', 'logs'], level: 'debug' },
  }
});

module.exports = logger.getLogger();