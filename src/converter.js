const exec = require('child_process').exec;
const log = require('../src/logger');
const fsExtra = require('fs-extra');
const { join, resolve } = require('path');

const getLibrePath = _ => {
  let librePaths = '';

  switch (process.platform) {
    case 'darwin':
      librePaths = ['/Applications/LibreOffice.app/Contents/MacOS/soffice'];
      break;
    case 'linux':
      librePaths = ['/usr/bin/libreoffice', '/usr/bin/soffice'];
      break;
    case 'win32':
      librePaths = [join(process.env[`PROGRAMFILES${process.arch === "x32" ? "(X86)" : ""}`], 'LIBREO~1/program/soffice.exe')];
      break;
  }

  return librePaths.find(path => fsExtra.pathExistsSync(path));
};
const LIBRE_PATH = getLibrePath();

if (!LIBRE_PATH) {
  log.error('LibreOffice dependency is missed on current machine. Please download and install it using https://www.libreoffice.org/download/download/');
  process.exit();
}

const presentationToPDF = async (inputFilePath, downloadPath) => {
  const DOWNLOADS_FOLDER = resolve('./', downloadPath); // Output file will be placed into downloads folder

  console.log(inputFilePath, '---------------', DOWNLOADS_FOLDER);
  if (this.activeConversionProcess) {
    await this.activeConversionProcess;
    log.info(`presentationToPDF(): Conversion process is busy, we'll wait...`);
  } // wait till process ends

  return this.activeConversionProcess = new Promise(async (resolve, reject) => {
    log.info(`presentationToPDF(): Preparing conversion parameters and configs...`);

    const cmd = `"${LIBRE_PATH}" --headless --norestore --nologo --convert-to pdf --outdir "${DOWNLOADS_FOLDER}" "${inputFilePath}"`;

    log.info(`presentationToPDF(): Conversion process begins... `);

    const fileInformation = {
      file: inputFilePath,
    };

    exec(cmd, async err => {
      if (err) {
        log.error(`presentationToPDF(): Conversion rejected! LibreOffice exception - ${err}`);
        reject(err);
        return;
      }
      if (await fsExtra.pathExists(inputFilePath)) {
        log.info(`presentationToPDF(): Conversion ended success! - ${inputFilePath}`);
        resolve(inputFilePath);
      } else {


        log.info(JSON.stringify(fileInformation));
        log.error(`presentationToPDF(): Conversion failed - cant access file via file system. No file, or file locked`);

        reject(`Error while converting file`);
      }
    });

    this.activeConversionProcess = null;
  })
};

module.exports = presentationToPDF;
