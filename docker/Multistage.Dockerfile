# syntax=docker/dockerfile:experimental

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

#------------------------------------------
# builder
#------------------------------------------
FROM node:10.21.0-jessie as builder

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN apt update && \
    apt install -y \
    git \
    openssl

# Setup ssh
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /app/workdir

COPY ./ ./

RUN npm set unsafe-perm true
RUN --mount=type=ssh,id=default npm i
RUN npm run build
RUN --mount=type=ssh,id=default npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git' -- -module recording-preview -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}


#------------------------------------------
# presentations
#------------------------------------------
FROM node:10.21.0-jessie

RUN apt-get update -y && apt-get install --no-install-recommends -y libreoffice libreoffice-dev && \
    apt-get clean

RUN npm i -g nodemon

WORKDIR /app/set2meet/presentations

RUN mkdir -p uploads downloads

COPY --from=builder /app/workdir/config ./config
COPY --from=builder /app/workdir/node_modules ./node_modules
COPY --from=builder /app/workdir/dist ./dist
COPY --from=builder /app/workdir/package.json ./package.json
COPY --from=builder /app/workdir/package-lock.json ./package-lock.json
COPY --from=builder /app/workdir/nodemon.json ./nodemon.json

EXPOSE 4000 4001
CMD nodemon --config nodemon.json
